﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using mvc16.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace mvc16.Controllers
{
    public class CartaController : Controller
    {
        /*Instancia de la clase escandalloContext (bbdd)*/
        private EscandalloContext db;

        /*Constructor de esta clase que inicializa la variable anterior*/
        public CartaController(EscandalloContext cont)
        {
            db = cont;
        }

        // GET: /<controller>/

        public IActionResult Ingredientes(int id = 0)
        {
            var query = db.platos.Where(o => o.Id == id).FirstOrDefault();
            List<Ingrediente> lista = query.Ingredientes.ToList();
            return View(lista);
        }

        public IActionResult Index()
        {
           // EscandalloContext ec = new EscandalloContext();
            //List<Plato> p = ec.platos.ToList();
            //List<Plato> p = new List<Plato>(); //el resultado de la DB
            //Plato p1 = new Models.Plato();
            //p1.Id = 1;
            //p1.Nombre = "Paella";

            //p.Add(p1);

            //Plato p2 = new Plato();
            //p2.Id = 2;
            //p2.Nombre = "Purrusalda";

            //p.Add(p2);

            return View(db.platos.ToList());
        }
    }
}
