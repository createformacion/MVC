﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using mvc16.Models;

namespace mvc16.Migrations
{
    [DbContext(typeof(EscandalloContext))]
    [Migration("20161116090210_mejora")]
    partial class mejora
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("mvc16.Models.Ingrediente", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Cantidad");

                    b.Property<string>("Nombre");

                    b.Property<int?>("PlatoId");

                    b.Property<string>("Receta");

                    b.Property<string>("Unidad");

                    b.HasKey("Id");

                    b.HasIndex("PlatoId");

                    b.ToTable("ingredientes");
                });

            modelBuilder.Entity("mvc16.Models.Plato", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Nombre");

                    b.Property<string>("Receta");

                    b.HasKey("Id");

                    b.ToTable("platos");
                });

            modelBuilder.Entity("mvc16.Models.Ingrediente", b =>
                {
                    b.HasOne("mvc16.Models.Plato")
                        .WithMany("Ingredientes")
                        .HasForeignKey("PlatoId");
                });
        }
    }
}
