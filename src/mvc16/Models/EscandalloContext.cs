﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mvc16.Models
{
    public class EscandalloContext : DbContext
    {
        public DbSet<Plato> platos { get; set; }
        public DbSet<Ingrediente> ingredientes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            string sConn = "Server=(localdb)\\mssqllocaldb;Database=escandalloDB";
            options.UseSqlServer(sConn);
        }
    }
}
