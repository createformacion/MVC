﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mvc16.Models
{
    public class Plato
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Receta { get; set; }
        public ICollection<Ingrediente> Ingredientes { get; set; }
    }
}
